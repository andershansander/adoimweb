import store from './store.js'

var sendTime;

var socket = new WebSocket("ws://localhost:8081/ws")
socket.onopen = () => {
  socket.send(JSON.stringify({
    type: "request-game-state"
  }))
}

socket.onmessage = (message) => {
  console.log("Message from server:", message)
  console.error("Received message after " + (Date.now() - sendTime) + "ms")
  store.dispatch({type: "game-update", payload: JSON.parse(message.data)})
}

export default function (command) {
  sendTime = Date.now()
  socket.send(JSON.stringify(command))
  console.error("Sending command took " + (Date.now() - sendTime) + "ms")
}