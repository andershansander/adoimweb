import ROT from "./vendor/rot.min.js"
var display = new ROT.Display()

display.setOptions({
	fg: "#C8BF4D",
    fontSize: 32,
})
document.body.appendChild(display.getContainer())

function drawLog(messages) {
    for (let i = 0; i < messages.length; i++) {
        let color = messages[i].secret ? "%c{#DDDDDD}" : "%c{#FFFFFF}"
        for (let j = 0; j < 40; j++) {
            display.draw(40 + j, i, "")
        }
        display.drawText(40, i, color + messages[i].text)
    }
}

var DEFAULT_COLOR = "%c{#FFFFFF}"

function color(terrain) {
    switch (terrain) {
    case "T":
        return "%c{#55A232}"
    case "#":
        return "%c{#555555}"
    case ".":
        return "%c{#186844}"
    case ",": 
        return "%c{#267C5B}"
    case "w": 
        return Math.random() > 0.7 ? "%c{#0066CC}": "%c{#0040EE}"
    case "W":
        return Math.random() > 0.7 ? "%c{#001855}": "%c{#001177}"
    }
    return DEFAULT_COLOR
}
function char(terrain) {
    switch (terrain) {
    case ",": 
    case "w":
    case "W":
        return "."
    }
    return terrain
}

function drawRow(row, rowIndex) {
    var lastType = null
    var homogenousPart = ""
    var resultRow = ""
    for (let i = 0; i < row.length; i++) {
        homogenousPart = color(row[i]) + char(row[i])
        while (i + 1 < row.length && row[i] === row[i + 1]) {
            i++;
            homogenousPart += char(row[i])
        }
        resultRow += homogenousPart
    }
    display.drawText(0, rowIndex, resultRow)
}

function drawActor(actor) {
    display.drawText(actor.location.x, actor.location.y, "%c{#FF0000}" + actor.sign)
}

export default function (state) {
    if (!state) {
        return
    }
    console.log("State: " + JSON.stringify(state))
    var time = Date.now()
    state.map.rows.forEach(drawRow)
    state.map.actors.forEach(drawActor)
    drawLog(state.messages.slice(-10))
    console.log("Drawing took " + (Date.now() - time) + "ms")
}
