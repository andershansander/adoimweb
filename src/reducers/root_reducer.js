import {Map, fromJS} from 'immutable'
import sendCommand from '../server_connection.js'
import move from '../action_handlers/move.js'

const INITIAL_STATE = {
}

const reducer = function(state = INITIAL_STATE, action) {
  console.debug(action)
  if (action.type === 'game-update') {
    return {
      game: action.payload
    }
  } else if (action.type === 'user-action') {
    if (action.command.type = "move") {
      sendCommand(move(action.command, state))
    } else {
      console.log("Unknown user action" + action.command.type)
    }
    
  }
  return state
}

export default reducer