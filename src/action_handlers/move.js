function translate(x, y, state, commandType) {
    if (state && state.game) {
        return {
            type: commandType,
            payload: {
                target: {
                    x: state.game.player.location.x + x,
                    y: state.game.player.location.y + y
                }
            }
        }
    }
    return {
        type: commandType,
        payload: {
            target: {
                x: 1, //state.map.actors[0].location.x - x,
                y: 2 //state.map.actors[0].location.y - y
            }
        }
    }
}

const toMoveCommand = function(command, state) {
  switch(command.payload.direction) {
    case "nw":
        return translate(-1, -1, state, command.type)
    case "n":
        return translate(0, -1, state, command.type)
    case "ne":
        return translate(+1, -1, state, command.type)
    case "e":
        return translate(+1, 0, state, command.type)
    case "se":
        return translate(+1, +1, state, command.type)
    case "s":
        return translate(0, +1, state, command.type)
    case "sw":
        return translate(-1, +1, state, command.type)
    case "w":
        return translate(-1, 0, state, command.type)
    default:
        console.error("Bad direction " + command.direction)
  }
}

export default toMoveCommand