import store from './store.js'
import './input/keyboard_handler.js'
import './server_connection.js'
import renderGame from './game_renderer.js'

require("./css/adoim.css")


store.subscribe(() => {
  renderGame(store.getState().game)
})
