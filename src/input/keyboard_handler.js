import store from '../store.js'

const keyCodeBindings = {
	inventory: {
		"27": {
            type: "hide-inventory"
        }
	},
	game: {
		
	}
};

const keyBindings = {
	inventory: {
		i: {
            type: "hide-inventory"
        }
	},
	game: {
		8: {type: "move", payload: {direction: "n"}},
		7: {type: "move", payload: {direction: "nw"}},
		9: {type: "move", payload: {direction: "ne"}},
		6: {type: "move", payload: {direction: "e"}},
		3: {type: "move", payload: {direction: "se"}},
		2: {type: "move", payload: {direction: "s"}},
		1: {type: "move", payload: {direction: "sw"}},
		4: {type: "move", payload: {direction: "w"}},

		i: {type: "show-inventory"}
	}
};

var keyInterpreter = {
    toCommand: function (keyCode) {
		return keyBindings.game[keyCode]
    }
}

const handleKeyUp = function (event) {
	event.preventDefault()

	console.debug("Key up event. KeyCode: " + event.keyCode)
    var command = keyInterpreter.toCommand(event.keyCode)
	if (command) {
        store.dispatch({
            type: "user-action",
            command: command
        })
    }
};

const handleKeyPress = function (event) {
	event.preventDefault();
	
	var keyPressed = String.fromCharCode(event.charCode);
	console.log("Key pressed. CharCode: " + event.charCode);
    var command = keyInterpreter.toCommand(keyPressed)
	if (command) {
        store.dispatch({
            type: "user-action",
            command: command
        })
    }
};

document.addEventListener("keyup", handleKeyUp);
document.addEventListener("keypress", handleKeyPress);